$(document).ready(function(){

    $(document).keypress(function(touche) {
        if(touche.which == 13) {
            let newTask = $("input").val();
            if(newTask!==""){
            $("#liste").append(`<li><span><i class="far fa-trash-alt"></i> </span> &nbsp Tache:  ${newTask} </li>`);
            newTask= $("#input").val(""); 
        }
        }
        
    });

    $("#myBtn").click(function(){
            let newTask = $("input").val();
            if(newTask!==""){
            $("#liste").append(`<li><span><i class="far fa-trash-alt"></i></span>  &nbsp Tache:  ${newTask} </li>`);
            newTask=  $("#input").val("");
        }
    });

    $("#liste").on('click',"li", function(){

            $(this).toggleClass("completed")
        });

    $("#liste").on('click','span',function(event){

        $(this).parent().fadeOut(500,function(){

            $(this).remove()    
        });
        event.stopPropagation()
        });
});
